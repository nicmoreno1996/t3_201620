package parqueadero;

import java.util.Date;
import java.util.Iterator;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	private Cola<Carro> colaDeCarros;
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */	
	private Pila<Carro>[] lasPilas;
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	private Pila<Carro> pilaTemporal;
   
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
    	//Carros esperando ser guardados en el parqueadero
        colaDeCarros = new Cola<Carro>();
        
        //Pilas de carros (parqueaderos de 1 a 8)
        lasPilas = new Pila[8];
        for(int i = 0; i < 8; i++)
        {
        	lasPilas[i] = new Pila<Carro>();
        }

        //Pila de carros temporal (carros pendientes para parquear)
        pilaTemporal = new Pila<Carro>();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro carroAIngresar = new Carro(pColor, pMatricula, pNombreConductor);
    	colaDeCarros.a�adir(carroAIngresar);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	if(colaDeCarros.darTamano() == 0)
    	{
    		throw new Exception("No hay carros esperando a ser parqueados.");
    	}
    	
    	Carro elCarro = colaDeCarros.sacar();
    	String laUbicacion = parquearCarro(elCarro);
    	
    	return elCarro.darMatricula() + "*" + laUbicacion;
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	Carro elCarro = sacarCarro(matricula);
    	double laCifra = cobrarTarifa(elCarro);
    	return laCifra;
    }
    
  /**
   * Busca un parqueadero con cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	boolean encontroParqueadero = false;
    	
    	int i = 0;
    	while(i < lasPilas.length && !encontroParqueadero)
    	{
    		if(lasPilas[i].darTamano() < 4)
    		{
    			lasPilas[i].push(aParquear);
    			encontroParqueadero = true;
    		}
    		
    		i++;
    	}
    	
    	if(encontroParqueadero == false)
    	{
    		throw new Exception("No hay parqueaderos disponibles para estacionar el carro.");
    	}
    	
    	return "Parqueadero:"+ " " + i;
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	Carro elCarro = null;
    	
    	if(sacarCarroP1(matricula) != null)
    	{
    		elCarro = sacarCarroP1(matricula);
    	}
    	else if(sacarCarroP2(matricula) != null)
    	{
    		elCarro = sacarCarroP2(matricula);
    	}
    	else if(sacarCarroP3(matricula) != null)
    	{
    		elCarro = sacarCarroP3(matricula);
    	}
    	else if(sacarCarroP4(matricula) != null)
    	{
    		elCarro = sacarCarroP4(matricula);
    	}
    	else if(sacarCarroP5(matricula) != null)
    	{
    		elCarro = sacarCarroP5(matricula);
    	}
    	else if(sacarCarroP6(matricula) != null)
    	{
    		elCarro = sacarCarroP6(matricula);
    	}
    	else if(sacarCarroP7(matricula) != null)
    	{
    		elCarro = sacarCarroP7(matricula);
    	}
    	if(sacarCarroP8(matricula) != null)
    	{
    		elCarro = sacarCarroP8(matricula);
    	}
    	
    	return elCarro;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[0].darTamano() != 0))
    	{
    		if(lasPilas[0].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[0].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[0].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA1 = pilaTemporal.pop();
    			lasPilas[0].push(carroA1);
    		}
    	}
    	
    	return elCarro;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[1].darTamano() != 0))
    	{
    		if(lasPilas[1].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[1].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[1].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA2 = pilaTemporal.pop();
    			lasPilas[1].push(carroA2);
    		}
    	}
    	
    	return elCarro;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[2].darTamano() != 0))
    	{
    		if(lasPilas[2].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[2].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[2].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA3 = pilaTemporal.pop();
    			lasPilas[2].push(carroA3);
    		}
    	}
    	
    	return elCarro;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[3].darTamano() != 0))
    	{
    		if(lasPilas[3].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[3].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[3].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA4 = pilaTemporal.pop();
    			lasPilas[3].push(carroA4);
    		}
    	}
    	
    	return elCarro;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[4].darTamano() != 0))
    	{
    		if(lasPilas[4].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[4].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[4].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA5 = pilaTemporal.pop();
    			lasPilas[4].push(carroA5);
    		}
    	}
    	
    	return elCarro;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[5].darTamano() != 0))
    	{
    		if(lasPilas[5].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[5].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[5].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA6 = pilaTemporal.pop();
    			lasPilas[5].push(carroA6);
    		}
    	}
    	
    	return elCarro;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[6].darTamano() != 0))
    	{
    		if(lasPilas[6].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[6].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[6].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA7 = pilaTemporal.pop();
    			lasPilas[6].push(carroA7);
    		}
    	}
    	
    	return elCarro;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	Carro elCarro = null;
    	boolean loEncontro = false;
    	
    	while(!loEncontro && (lasPilas[7].darTamano() != 0))
    	{
    		if(lasPilas[7].darItemArriba().darMatricula().equals(matricula))
    		{
    			loEncontro = true;
    			elCarro = lasPilas[7].pop();
    		}
    		else
    		{
    			Carro carroEnTemp = lasPilas[7].pop();
    			pilaTemporal.push(carroEnTemp);
    		}
    	}
    	
    	if(pilaTemporal.darTamano() != 0)
    	{
    		while(pilaTemporal.darTamano() != 0)
    		{
    			Carro carroA8 = pilaTemporal.pop();
    			lasPilas[7].push(carroA8);
    		}
    	}
    	
    	return elCarro;
    }
    
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	Date fechaSalida = new Date();
    	long horaSalidaEnMili = fechaSalida.getTime();
    	
    	long tiempoDentroDeParqudero = horaSalidaEnMili - car.darLlegada();
    	
    	double tiempoEnMinutos = (tiempoDentroDeParqudero * (1/1000)*(1/60));
    	
    	return tiempoEnMinutos*25;	
    }
    
    public Pila<Carro> darPilas(int i)
    {
    	return lasPilas[i];
    }
    
    public Cola<Carro> darCola()
    {
    	return colaDeCarros;
    }
}
