package parqueadero;

public class Cola<T>
{
	private int tamano;
	private NodoCola<T> primero;
	private NodoCola<T> ultimo;
	
	public Cola()
	{
		primero = null;
		ultimo = null;
		tamano = 0;
	}
	
	public int darTamano()
	{
		return tamano;
	}
	
	public void aņadir(T pItem)
	{
		NodoCola<T> elUltimoAnterior = ultimo;
		ultimo = new NodoCola<T>(pItem, null);
		if(estaVacia())
		{
			primero = ultimo;
		}
		else
		{
			elUltimoAnterior.aņadirSiguiente(ultimo);
		}
		tamano++;
	}
	
	public T sacar()
	{
		T elItem = null;
		if(estaVacia())
		{
			return null;
		}
		else if(!estaVacia())
		{
			elItem = primero.darItem();
			primero = primero.darSiguiente();
		}
		
		tamano--;
		return elItem;
	}

	public boolean estaVacia()
	{
		return primero == null;
	}
	
	@Override
	public String toString( )
	{
		String elMensaje = "[" + tamano + "]:";
		for( NodoCola<T> elNodo = primero; elNodo != null; elNodo = elNodo.darSiguiente( ) )
		{
			elMensaje += elNodo.darItem().toString( ) + " - ";
		}
		return elMensaje;
	}
}
