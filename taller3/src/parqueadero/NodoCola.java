package parqueadero;

public class NodoCola<T>
{
	private NodoCola<T> siguiente;
	private T item;
	
	public NodoCola(T pItem, NodoCola<T> pSiguiente)
	{
		siguiente = pSiguiente;
		item = pItem;
	}
	
	public T darItem()
	{
		return item;
	}
	
	public NodoCola<T> darSiguiente()
	{
		return siguiente;
	}
	
	public void aņadirSiguiente(NodoCola<T> pNodo)
	{
		 siguiente = pNodo;
	}
	
	public String toString()
	{
		return item.toString();
	}
}
