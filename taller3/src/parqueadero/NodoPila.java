package parqueadero;

public class NodoPila<T>
{
	private NodoPila<T> siguiente;
	private T item;
	
	public NodoPila(T pItem, NodoPila<T> pSiguiente)
	{
		siguiente = pSiguiente;
		item = pItem;
	}
	
	public T darItem()
	{
		return item;
	}
	
	public NodoPila<T> darSiguiente()
	{
		return siguiente;
	}
	
	public String toString()
	{
		return item.toString();
	}
}
