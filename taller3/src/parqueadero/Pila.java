package parqueadero;

public class Pila<T>
{
	private int tamano;
	private NodoPila<T> primero;
	
	public Pila()
	{
		this.primero = null;
		this.tamano = 0;
	}
	
	public int darTamano()
	{
		return tamano;
	}
	
	public T darItemArriba()
	{
		if(estaVacia())
		{
			return null;
		}
		return primero.darItem();
	}
	
	public boolean estaVacia()
	{
		return primero == null;
	}
	
	public void push(T item)
	{
		if(primero == null)
		{
			primero = new NodoPila<T>(item, null);
		}
		else
		{
			NodoPila<T> temp = primero;
			primero = new NodoPila<T>(item, temp);
		}
		tamano++;
	}
	
	public T pop()
	{
		T elItem = null;
		if(primero != null)
		{
			elItem = primero.darItem();
			primero = primero.darSiguiente();
			tamano--;
		}
		return elItem;
	}
	
	@Override
	public String toString( )
	{
		String elMensaje = "[" + tamano + "]:";
		for( NodoPila<T> elNodo = primero; elNodo != null; elNodo = elNodo.darSiguiente( ) )
		{
			elMensaje += elNodo.darItem().toString( ) + " - ";
		}
		return elMensaje;
	}
}
